//
//  EditRegisterViewController.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 20/02/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import UIKit
import DropDown

class EditRegisterViewController: UIViewController {

    
    var dropDownSex = DropDown()
    var dropDownSchooling = DropDown()
    var dropDownIcome = DropDown()
    var dropDownReligion = DropDown()
    var dropDownAffectiveSituation = DropDown()
    var dropDownChildren = DropDown()
    var dropDownEmploymentSituation = DropDown()
    var dropDownMaritalStatus = DropDown()
    
    
    var sexOptions = ["Masculino","Femenino"]
    var schoolingOptions =
        ["Fundamental - Incompleto",
         "Fundamental - Completo",
         "Médio - Incompleto",
         "Médio - Completo",
         "Superior - Incompleto",
         "Superior - Completo",
         "Pós-graduação - Incompleto",
         "Pós-graduação - Completo",
         "Mestrado - Incompleto",
         "Mestrado - Completo",
         "Doutorado - Incompleto",
         "Doutorado - Completo"]
    var icomeOptions = ["Alta","Media","Baixa"]
    var religionOptions = ["Catolico","Evangelico","Espirita","Budista","Islamista","Judeu","Umbandista","Agnostico","Ateu"]
    var maritalStatusOptions = ["Solteiro", "Casado", "Divorciado", "União estavél", "Viuvo"]
    var affectiveSituationOptions = ["Solteiro", "Namorando", "Enrolado"]
    var childrenOptions = ["Sim", "Não"]
    var employmentSituationOptions = ["Empregado", "Desempregado"]
    var user = User()
    override func viewDidLoad() {
        super.viewDidLoad()

        dropDownSex = DropDownUtil.getDropDown(dropDown:dropDownSex, anchor: self.editSexField,options: sexOptions,dropDownButton: self.sexButton, context:self,user: user,campo: "sex")!
        
        dropDownSchooling = DropDownUtil.getDropDown(dropDown:dropDownSchooling, anchor: self.schoolingField,options: schoolingOptions,dropDownButton: self.schoolingButton, context:self,user: user,campo: "schooling")!
        
        dropDownIcome = DropDownUtil.getDropDown(dropDown:dropDownIcome, anchor: self.icomeField,options: icomeOptions,dropDownButton: self.icomeButton, context:self,user: user,campo: "icome")!
        
        dropDownReligion = DropDownUtil.getDropDown(dropDown:dropDownReligion, anchor: self.religionField,options: religionOptions,dropDownButton: self.religionButton, context:self,user: user,campo: "religion")!
        
        dropDownAffectiveSituation = DropDownUtil.getDropDown(dropDown:dropDownAffectiveSituation, anchor: self.affectiveSituationField,options: affectiveSituationOptions,dropDownButton: self.affectiveSituationButton, context:self,user: user,campo: "affectiveSituation")!
        
        dropDownChildren = DropDownUtil.getDropDown(dropDown:dropDownChildren, anchor: self.childrenField,options: childrenOptions,dropDownButton: self.childrenButton, context:self, user: user,campo: "children")!
        
        dropDownEmploymentSituation = DropDownUtil.getDropDown(dropDown:dropDownEmploymentSituation, anchor: self.employmentSituationField,options: employmentSituationOptions,dropDownButton: self.employmentSituationButton, context:self, user: user,campo: "employmentSituation")!
        
        dropDownMaritalStatus = DropDownUtil.getDropDown(dropDown:dropDownMaritalStatus, anchor: self.maritalStatusField,options: maritalStatusOptions,dropDownButton: self.maritalStatusButton, context:self, user: user,campo: "maritalStatus")!
        scrollView.contentSize.height = 644
    }
    
    @IBOutlet weak var editNameField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var editMailField: UITextField!
    @IBOutlet weak var editPasswordField: UITextField!
    @IBOutlet weak var editconfirmPasswordField: UITextField!
    @IBOutlet weak var editSexField: UIView!
    @IBOutlet weak var schoolingField: UIView!
    @IBOutlet weak var icomeField: UIView!
    @IBOutlet weak var religionField: UIView!
    @IBOutlet weak var editBirthField: UITextField!
    @IBOutlet weak var affectiveSituationField: UIView!
    @IBOutlet weak var childrenField: UIView!
    @IBOutlet weak var employmentSituationField: UIView!
    @IBOutlet weak var maritalStatusField: UIView!
    
    
    @IBOutlet weak var sexButton: UIButton!
    @IBAction func sexButton(_ sender: Any) {
        dropDownSex.show()
    }
    
    @IBOutlet weak var schoolingButton: UIButton!
    @IBAction func schoolingButton(_ sender: Any) {
        dropDownSchooling.show()
    }
    
    @IBOutlet weak var icomeButton: UIButton!
    @IBAction func icomeButton(_ sender: Any) {
        dropDownIcome.show()
    }
    
    @IBOutlet weak var religionButton: UIButton!
    @IBAction func religionButton(_ sender: Any) {
        dropDownReligion.show()
    }
    
    @IBOutlet weak var maritalStatusButton: UIButton!
    @IBAction func maritalStatusButton(_ sender: Any) {
        dropDownMaritalStatus.show()
    }
    
    @IBOutlet weak var affectiveSituationButton: UIButton!
    @IBAction func affectiveSituationButton(_ sender: Any) {
        dropDownAffectiveSituation.show()
    }
    @IBOutlet weak var childrenButton: UIButton!
    @IBAction func childrenButton(_ sender: Any) {
        dropDownChildren.show()
    }
    
    @IBOutlet weak var employmentSituationButton: UIButton!
    @IBAction func employmentSituationButton(_ sender: Any) {
        dropDownEmploymentSituation.show()
    }
    
    func setUser(user:User) -> User {
        user.name = editNameField.text!
        user.email = editMailField.text!
        user.password = editPasswordField.text!
        user.dateOfBirth = editBirthField.text!
        return user
    }
}
