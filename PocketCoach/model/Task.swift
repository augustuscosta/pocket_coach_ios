
//
//  Task.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 20/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import ObjectMapper

class Task: Object {
    
    var id = ""
    var descricao = ""
    var status = ""
    var typeFrequency = ""
    var dateRealization = ""
    var goal: Goal? = Goal()
    var goalId = 0
    var hour = ""
    var weekDays = [String]()
    var dayOfMonth = 0


    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Task : Mappable {
    func mapping(map: Map){
        id <- map["id"]
        descricao <- map["description"]
        status <- map["status"]
        typeFrequency <- map["type_frequency"]
        dateRealization <- map["date_realization"]
        goal <- map["goal"]
        goalId <- map["goal_id"]
        hour <- map["hour"]
        weekDays <- map["goal_id"]
        dayOfMonth <- map["day_of_month"]
    }
}

extension Task: RealmGenericDAO {
    
    static let realm = try! Realm()
    
    static func save(object: Task) {
        try! realm.write {
            realm.add(object, update: true)
        }
    }
    
    static func saveAll(objects: [Task]) {
        try! realm.write {
            realm.add(objects, update: true)
        }
    }
    
    static func getByID(id: AnyObject) -> Task? {
        return realm.object(ofType: Task.self, forPrimaryKey: id)
    }
    
    static func all() -> [Task]? {
        return [Task](realm.objects(Task.self))
    }

}
