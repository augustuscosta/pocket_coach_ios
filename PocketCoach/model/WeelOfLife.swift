//
//  WeelOfLife.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 20/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import ObjectMapper

class WeelOfLife: Object {
    
    var id = 0
    var totalPersonal: Float = 0.0
    var totalProfessional: Float = 0.0
    var totalQualitLife: Float = 0.0
    var emotionalBalance = 0
    var healthAndDisposition = 0
    var intellectualDevelopment = 0
    var careerAndBusiness = 0
    var finances = 0
    var socialContribution = 0
    var recreation = 0
    var satisfaction = 0
    var spirituality = 0
    var family = 0
    var relationshipWithLove = 0
    var socialLife = 0
    
    var user = List<User>()
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension WeelOfLife : Mappable {
    func mapping(map: Map){
        id <- map["id"]
        totalPersonal <- map["total_personal"]
        totalProfessional <- map["total_relationships"]
        totalQualitLife <- map["total_qualit_life"]
        emotionalBalance <- map["emotional_balance"]
        healthAndDisposition <- map["health_and_disposition"]
        intellectualDevelopment <- map["intellectual_development"]
        careerAndBusiness <- map["career_and_business"]
        finances <- map["finances"]
        socialContribution <- map["social_contribution"]
        recreation <- map["recreation"]
        satisfaction <- map["satisfaction_and_fullfillment"]
        spirituality <- map["spirituality"]
        family <- map["family"]
        relationshipWithLove <- map["relationship_with_love"]
        socialLife <- map["socialLife"]
        user <- map["user"]
    }
}

extension WeelOfLife: RealmGenericDAO {
    
    static func save(object: WeelOfLife) {
        let realm = try! Realm()
        do {
            try realm.write{
                realm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o jogo")
        }
    }
    
    static func saveAll(objects: [WeelOfLife]) {
        let realm = try! Realm()
        do {
            try realm.write {
                realm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os jogos")
        }
    }
    
    static func getByID(id: AnyObject) -> WeelOfLife? {
        let realm = try! Realm()
        
        let results = realm.objects(WeelOfLife.self).filter("id = %@", id)
        return results.first
    }
    
    static func all() -> [WeelOfLife]? {
        let realm = try! Realm()
        
        var list: [WeelOfLife] = []
        list.append(contentsOf: realm.objects(WeelOfLife.self))
        return list
    }
    
}

