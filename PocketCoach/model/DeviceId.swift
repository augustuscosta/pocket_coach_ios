//
//  DeviceId.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 20/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class DeviceId {
    
    var os = "IOS"
    var token = ""
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
}

extension DeviceId : Mappable {
    func mapping(map: Map){
        os <- map["os"]
        token <- map["token"]
        
    }
}


