//
//  Goal.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 20/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import ObjectMapper

class Goal: Object {
    
    var id = 0
    var definition = ""
    var dateRealization = ""
    var calculateStatus = 0.0
    var specific = false
    var temporal = false
    var relevant = false
    var reachable = false
    var measurable = false
    var ecological = false
    var status = ""
    var image = ""
    var userId = 0
    var user: User? = User()
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Goal : Mappable {
    func mapping(map: Map){
        id <- map["id"]
        definition <- map["description"]
        dateRealization <- map["date_realization"]
        calculateStatus <- map["calculate_status"]
        specific <- map["specific"]
        temporal <- map["temporal"]
        relevant <- map["relevant"]
        reachable <- map["reachable"]
        measurable <- map["measurable"]
        ecological <- map["ecological"]
        image <- map["image"]
        status <- map["status"]
        userId <- map["user_id"]
        user <- map["user"]
    }
}

extension Goal: RealmGenericDAO {
    
    static func save(object: Goal) {
        let realm = try! Realm()
        do {
            try realm.write{
                realm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o jogo")
        }
    }
    
    static func saveAll(objects: [Goal]) {
        let realm = try! Realm()
        do {
            try realm.write {
                realm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os jogos")
        }
    }
    
    static func getByID(id: AnyObject) -> Goal? {
        let realm = try! Realm()
        
        let results = realm.objects(Goal.self).filter("id = %@", id)
        return results.first
    }
    
    static func all() -> [Goal]? {
        let realm = try! Realm()
        
        var list: [Goal] = []
        list.append(contentsOf: realm.objects(Goal.self))
        return list
    }
    
}


