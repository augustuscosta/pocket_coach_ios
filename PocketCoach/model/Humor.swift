//
//  Humor.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 20/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import ObjectMapper

class Humor: Object {
    
    var id = ""
    var userId = 0
    var name = ""
    var date = ""
    var user: User? = User()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Humor : Mappable {
    func mapping(map: Map){
        id <- map["id"]
        userId <- map["user_id"]
        name <- map["name"]
        date <- map["date"]
        user <- map["user"]
    }
}

extension Humor: RealmGenericDAO {
    
    static let realm = try! Realm()
    
    static func save(object: Humor) {
        try! realm.write {
            realm.add(object, update: true)
        }
    }
    
    static func saveAll(objects: [Humor]) {
        try! realm.write {
            realm.add(objects, update: true)
        }
    }
    
    static func getByID(id: AnyObject) -> Humor? {
        return realm.object(ofType: Humor.self, forPrimaryKey: id)
    }
    
    static func all() -> [Humor]? {
        return [Humor](realm.objects(Humor.self))
    }
    
}

