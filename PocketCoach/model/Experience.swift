//
//  Title.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 20/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import ObjectMapper

class Experience: Object {
    
    var id = 0
    var name = ""
    var user = List<User>()
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Experience : Mappable {
    func mapping(map: Map){
        id <- map["id"]
        name <- map["name"]
        user <- map["user"]
        
    }
}


extension Experience: RealmGenericDAO {
    
    static func save(object: Experience) {
        let realm = try! Realm()
        do {
            try realm.write{
                realm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o jogo")
        }
    }
    
    static func saveAll(objects: [Experience]) {
        let realm = try! Realm()
        do {
            try realm.write {
                realm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os jogos")
        }
    }
    
    static func getByID(id: AnyObject) -> Experience? {
        let realm = try! Realm()
        
        let results = realm.objects(Experience.self).filter("id = %@", id)
        return results.first
    }
    
    static func all() -> [Experience]? {
        let realm = try! Realm()
        
        var list: [Experience] = []
        list.append(contentsOf: realm.objects(Experience.self))
        return list
    }
    
}
