//
//  User.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 20/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import ObjectMapper

class User: Object {
    
    var id = 0
    var name = ""
    var email = ""
    var dateOfBirth = ""
    var password = ""
    var avatar = ""
    var currentUser = false
    var facebookAccount = false
    var facebookToken = ""
    var schooling = ""
    var child = ""
    var gender = ""
    var rent = ""
    var religion = ""
    var affectiveSituation = ""
    var employmentSituation = ""
    var accountTypeId = 0
    var experienceId = 0
    var wheelOfLifes = List<WeelOfLife>()
    var humors = List<Humor>()
    var goals = List<Goal>()
    var temperaments = List<Temperament>()
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension User : Mappable {
    func mapping(map: Map){
        
        id <- map["id"]
        name <- map["name"]
        dateOfBirth <- map["date_of_birth"]
        email <- map["email"]
        password <- map["password"]
        avatar <- map["avatar"]
        facebookAccount <- map["facebook_account"]
        facebookToken <- map["facebook_token"]
        schooling <- map["schooling"]
        child <- map["child"]
        rent <- map["rent"]
        gender <- map["gender"]
        religion <- map["religion"]
        affectiveSituation <- map["affective_situation"]
        employmentSituation <- map["employment_situation"]
        accountTypeId <- map["account_type_id"]
        experienceId <- map["experience_id"]
        wheelOfLifes <- map["wheels"]
        humors <- map["humors"]
        goals <- map["goals"]
        temperaments <- map["temperaments"]
    }
}

extension User: RealmGenericDAO {
    static let realm = try! Realm()
    
    static func save(object: User) {
        try! realm.write {
            realm.add(object, update: true)
        }
    }
    
    static func saveAll(objects: [User]) {
        try! realm.write {
            realm.add(objects, update: true)
        }
    }
    
    static func getByID(id: AnyObject) -> User? {
        return realm.object(ofType: User.self, forPrimaryKey: id)
    }
    
    static func all() -> [User]? {
        return [User](realm.objects(User.self))
    }
    
}

