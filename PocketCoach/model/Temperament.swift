//
//  Temperament.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 20/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import ObjectMapper

class Temperament: Object {
    
    var id = ""
    var result = ""
    var date: NSDate? = nil
    var willResult = 0
    var wishResult = 0
    var furyResult = 0
    var inhibitionResult = 0
    var sensibilityResult = 0
    var copingResult = 0
    var controlResult = 0
    var optimist = 0
    var easyPleasure = 0
    var animate = 0
    var selfEsteem = 0
    var enthusiastic = 0
    var motivated = 0
    var objective = 0
    var energetic = 0
    var strongImpulse = 0
    var exaggerateILike = 0
    var surrenderingToPleasure = 0
    var crazyIWantSomething = 0
    var immediatist = 0
    var extremist = 0
    var obstinate = 0
    var impatient = 0
    var angry = 0
    var aggressive = 0
    var explosive = 0
    var distrustful = 0
    var audacious = 0
    var spontaneous = 0
    var unpreoccupied = 0
    var reagent = 0
    var careless = 0
    var impulsive = 0
    var imprudent = 0
    var risk = 0
    var guilty = 0
    var rejected = 0
    var criticism = 0
    var hurt = 0
    var trauma = 0
    var stress = 0
    var pressure = 0
    var frustration = 0
    var assumeGuilt = 0
    var toFaceProblem = 0
    var resolveProblem = 0
    var resolveProblemNow = 0
    var resolveConflict = 0
    var findSolution = 0
    var learnMyError = 0
    var painMakeStrong = 0
    var attentive = 0
    var focused = 0
    var planActivity = 0
    var concludesTask = 0
    var organized = 0
    var disciplined = 0
    var responsible = 0
    var perfectionist = 0
    var itemA = 0
    var itemB = 0
    var itemC = 0
    var itemD = 0
    var itemE = 0
    var itemF = 0
    var itemG = 0
    var itemH = 0
    var itemI = 0
    var itemJ = 0
    var itemK = 0
    var itemL = 0
    var itemLooksMe = 0
    var advantageHumor = 0
    var prejudiceHumor = 0
    var finished = false
    var userId = 0

    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Temperament : Mappable {
    func mapping(map: Map){
        id <- map["id"]
        result <- map["result"]
        date <- map["date"]
        willResult <- map["will_result"]
        wishResult <- map["wish_result"]
        furyResult <- map["fury_result"]
        inhibitionResult <- map["inhibition_result"]
        sensibilityResult <- map["sensibility_result"]
        copingResult <- map["coping_result"]
        controlResult <- map["control_result"]
        optimist <- map["optimist"]
        easyPleasure <- map["easy_pleasure"]
        animate <- map["animate"]
        selfEsteem <- map["self_esteem"]
        enthusiastic <- map["enthusiastic"]
        motivated <- map["motivated"]
        objective <- map["objective"]
        energetic <- map["energetic"]
        strongImpulse <- map["strong_impulse"]
        exaggerateILike <- map["exaggerate_i_like"]
        surrenderingToPleasure <- map["surrendering_to_pleasure"]
        crazyIWantSomething <- map["crazy_i_want_something"]
        immediatist <- map["immediatist"]
        extremist <- map["extremist"]
        immediatist <- map["immediatist"]
        obstinate <- map["obstinate"]
        impatient <- map["impatient"]
        angry <- map["angry"]
        aggressive <- map["aggressive"]
        explosive <- map["explosive"]
        distrustful <- map["distrustful"]
        audacious <- map["audacious"]
        spontaneous <- map["spontaneous"]
        unpreoccupied <- map["unpreoccupied"]
        reagent <- map["reagent"]
        careless <- map["careless"]
        impulsive <- map["impulsive"]
        risk <- map["risk"]
        guilty <- map["guilty"]
        rejected <- map["rejected"]
        criticism <- map["criticism"]
        hurt <- map["hurt"]
        trauma <- map["trauma"]
        stress <- map["stress"]
        pressure <- map["pressure"]
        frustration <- map["frustration"]
        assumeGuilt <- map["assume_guilt"]
        toFaceProblem <- map["to_face_problem"]
        resolveProblem <- map["resolve_problem"]
        resolveProblemNow <- map["resolve_problem_now"]
        resolveConflict <- map["resolve_conflict"]
        findSolution <- map["find_solution"]
        learnMyError <- map["learn_my_error"]
        painMakeStrong <- map["pain_make_strong"]
        attentive <- map["attentive"]
        painMakeStrong <- map["pain_make_strong"]
        focused <- map["focused"]
        planActivity <- map["plan_activity"]
        concludesTask <- map["concludes_task"]
        organized <- map["organized"]
        disciplined <- map["disciplined"]
        responsible <- map["responsible"]
        perfectionist <- map["perfectionist"]
        itemA <- map["item_a"]
        itemB <- map["item_b"]
        itemC <- map["item_c"]
        itemD <- map["item_d"]
        itemE <- map["item_e"]
        itemF <- map["item_f"]
        itemG <- map["item_g"]
        itemH <- map["item_h"]
        itemI <- map["item_i"]
        itemJ <- map["item_j"]
        itemK <- map["item_k"]
        itemL <- map["item_l"]
        itemLooksMe <- map["item_looks_me"]
        advantageHumor <- map["advantage_humor"]
        prejudiceHumor <- map["prejudice_humor"]
        userId <- map["user_id"]
    }
}


extension Temperament: RealmGenericDAO {
    
    static let realm = try! Realm()
    
    static func save(object: Temperament) {
        try! realm.write {
            realm.add(object, update: true)
        }
    }
    
    static func saveAll(objects: [Temperament]) {
        try! realm.write {
            realm.add(objects, update: true)
        }
    }
    
    static func getByID(id: AnyObject) -> Temperament? {
        return realm.object(ofType: Temperament.self, forPrimaryKey: id)
    }
    
    static func all() -> [Temperament]? {
        return [Temperament](realm.objects(Temperament.self))
    }
}
