//
//  NewGoalViewController.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 08/02/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import UIKit
import Alamofire

class NewGoalViewController: BaseViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    
    
    
    @IBOutlet weak var dateGoalField: UITextField!
    @IBOutlet weak var goalField: UITextField!
    @IBOutlet weak var specificSwitch: UISwitch!
    @IBOutlet weak var measurableSwitch: UISwitch!
    @IBOutlet weak var reachableSwitch: UISwitch!
    @IBOutlet weak var relevantSwitch: UISwitch!
    @IBOutlet weak var importantSwitch: UISwitch!
    @IBOutlet weak var ecologicalSwitch: UISwitch!
    @IBOutlet weak var goalImageButton: UIButton!
    
    var goal:Goal!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSlideMenuButton()
        self.title = "Novo objetivo"
        goal = Goal()
        
    }
    
    @IBAction func setGoalImage(_ sender: Any) {
        AlertUtil.showAlertPhotolibraryOrCamera(title: "Imagem", message: "Escolha a fonte da imagem", context: self)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        goalImageButton.setImage(chosenImage, for: UIControlState.normal)
        goalImageButton.setTitle(chosenImage.description, for: UIControlState.normal)
        dismiss(animated:true, completion: nil)
    }
    
    @IBAction func specificSwitchChanged(_ sender: UISwitch) {
        specificSwitch = sender
        showAlertSwitch(switchChenged: specificSwitch)
    }
    
    @IBAction func measurableSwitchChanged(_ sender: UISwitch) {
        measurableSwitch = sender
        showAlertSwitch(switchChenged: measurableSwitch)
    }
    
    @IBAction func reachableSwitchChanged(_ sender: UISwitch) {
        reachableSwitch = sender
        showAlertSwitch(switchChenged: reachableSwitch)
    }
    
    @IBAction func relevantSwitchChanged(_ sender: UISwitch) {
        relevantSwitch = sender
        showAlertSwitch(switchChenged: relevantSwitch)
    }
    
    @IBAction func importantSwitchChanged(_ sender: UISwitch) {
        importantSwitch = sender
        showAlertSwitch(switchChenged: importantSwitch)
    }
    
    @IBAction func ecologicalSwitchChanged(_ sender: UISwitch) {
        ecologicalSwitch = sender
        showAlertSwitch(switchChenged: ecologicalSwitch)
    }
    
    func showAlertSwitch(switchChenged: UISwitch){
        
        switch (switchChenged){
            
        case specificSwitch:
            if switchChenged.isOn != true{
                AlertUtil.showAlert(title: "Específico", message: "Quando uma meta é específica, significa que tira qualquer possibilidade de ambiguidade de seu entendimento", context: self)
            }
            break
        case measurableSwitch:
            if switchChenged.isOn != true{
                AlertUtil.showAlert(title: "Mensurável", message: "Toda meta precisa ser mensurável. Se a meta não pode ser medida quer dizer que ela nunca será alcançada", context: self)
            }
            break
        case reachableSwitch:
            if switchChenged.isOn != true{
                AlertUtil.showAlert(title: "Alcançável", message: "Ter uma meta difícil demais pode desmotiva-lo, assim como uma fácil demais. A difícil pode parecer inalcançável e a fácil que você se subestima", context: self)
            }
            break
        case relevantSwitch:
            if switchChenged.isOn != true{
                AlertUtil.showAlert(title: "Relevante", message: "Uma boa meta é aquela que traz resultados. Alguns criterios que podem ser levados em consideração são: o impacto nas métricas, timing e se o conjunto de metas faz sentido para o planejamento da sua vida", context: self)
            }
            break
        case importantSwitch:
            if switchChenged.isOn != true{
                AlertUtil.showAlert(title: "Temporal", message: "As metas são passos de um planejamento completo. Por esse motivo é importante colocar prazo em cada um dos passos. Se você não organizar o seu tempo com toda certeza, o final será prejudicado", context: self)
            }
            break
        case ecologicalSwitch:
            if switchChenged.isOn != true{
                AlertUtil.showAlert(title: "Ecologico", message: "Objetivos que não visão apenas beneficios próprios, mas que também proporcionam beneficios às pessoas e ao mundo ao seu redor, tem maior chance de serem bem sucedidos", context: self)
            }
            break
            
        default: break
        }
    }
    
    
    @IBAction func createNewGoal(_ sender: Any) {
        createGoal()
    }
    func createGoal(){
        
        var parameters = [String:String]()
        parameters["description"] = self.goalField.text
        parameters["date_realization"] = self.dateGoalField.text
        
        
        
        Alamofire.upload(multipartFormData: { data in
            if self.goalImageButton != nil {
                data.append(UIImageJPEGRepresentation(self.goalImageButton.image(for: UIControlState.normal)!,1)!, withName: "image_goal", fileName: "goal.jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in parameters {
                data.append(value.data(using: .utf8)!, withName: key)
            }
        }, to: "https://secure-plains-94243.herokuapp.com/api/create_goal_image.json", encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                   debugPrint(response)
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
        
    }
    
    
    
}
