//
//  MyMoodProfileViewController.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 02/02/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import UIKit
import Charts

class MyMoodProfileViewController: UIViewController {
    
    
    var months: [String]!
    
    @IBOutlet weak var barChartView: BarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
        
        setChart(dataPoints: months, values: unitsSold)
        
        
    }
        
//        let chartConfig = BarsChartConfig(
//            valsAxisConfig: ChartAxisConfig(from: 0, to: 8, by: 1)
//        )
//        
//         let chart = BarsChart(
//            frame: CGRect(x:40, y:220, width:220, height:180),
//            chartConfig: chartConfig,
//            xTitle: "",
//            yTitle: "",
//            bars: [
//                ("A", 2),
//                ("B", 4.5),
//                ("C", 3),
//                ("D", 5.4),
//                ("E", 6.8),
//                ("F", 0.5)
//            ],
//            color: UIColor.red,
//            barWidth: 20
//        )
//        self.view.addSubview(chart.view)
//        self.chart = chart
        

        // Do any additional setup after loading the view.
    

//    var chart:BarsChart!
    
func setChart(dataPoints: [String], values: [Double]) {
    barChartView.noDataText = "You need to provide data for the chart."
    var dataEntries: [BarChartDataEntry] = []
    
    for i in 0..<dataPoints.count {
        let dataEntry = BarChartDataEntry(x: Double(i), y:values[i])
        dataEntries.append(dataEntry)
    }
    
    let chartDataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
    let chartData = BarChartData(dataSet: chartDataSet)
    barChartView.data = chartData
    
}










}
