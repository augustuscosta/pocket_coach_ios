//
//  WheelOfLifeViewController.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 02/02/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import UIKit
import Charts

class WheelOfLifeViewController: UIViewController {

    @IBOutlet weak var pieChartsView: BarChartView!
    var months: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
        
        setChart(dataPoints: months, values: unitsSold)

        
    }

    func setChart(dataPoints: [String], values: [Double]) {
        pieChartsView.noDataText = "You need to provide data for the chart."
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y:values[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
        let chartData = BarChartData(dataSet: chartDataSet)
        pieChartsView.data = chartData
        
    }

}
