//
//  GoalsTableViewController.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 06/02/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class GoalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var goalImage: UIImageView!
    @IBOutlet weak var goalName: UILabel!
    @IBOutlet weak var goalDate: UILabel!
    @IBOutlet weak var goalProgress: UIProgressView!
    @IBOutlet weak var progressView: UIProgressView!
}

class GoalsTableViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    var goals = [Goal]()
    let URL = Util.rootUrl + "goals.json"
    let rootUrl = Util.root
    @IBOutlet weak var goalsTableView: UITableView!
    var imageToPass:UIImageView!
    var valuesToPass = [String]()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.addSlideMenuButton()
        self.title = "Objetivos"
        getGoals()
        goalsTableView.delegate = self
        goalsTableView.dataSource = self
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return goals.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellGoals", for: indexPath) as! GoalTableViewCell
        
        let goal = self.goals[indexPath.row]
        let progress = Float(goal.calculateStatus/100)
        cell.goalName?.text = goal.definition
        cell.goalDate.text? = goal.dateRealization
        cell.progressView.setProgress(progress, animated: true)
        loadImage(goal: goal, URL: rootUrl + goal.image, cell: cell)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        let currentCell = tableView.cellForRow(at: indexPath!) as! GoalTableViewCell
        
        valuesToPass.append(currentCell.goalName.text!)
        valuesToPass.append(currentCell.goalDate.text!)
        imageToPass = (currentCell.goalImage)!
        if (self.imageToPass != nil && valuesToPass.count > 0) {
            self.performSegue(withIdentifier: "goalDetail", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "goalDetail") {
            
            let viewController = segue.destination as! GoalViewController
            if imageToPass != nil {
                viewController.passedImage = imageToPass
            }
            if valuesToPass.count > 0 {
                viewController.passedValues = valuesToPass
            }
        }
    }
    
    func getGoals(){
        
        Alamofire.request(URL).responseArray { (response: DataResponse<[Goal]>) in
            
            let statusCode = ((response.response?.statusCode))!
            
            if statusCode == 200 {
                self.goals.append(contentsOf: response.result.value!)
                self.goalsTableView.reloadData()
            } else {
                AlertUtil.showAlert(title: "Falha",message: "Nao foi possivel carregar os objetivos", context: self)
            }
        }
    }
    
    
    func loadImage(goal:Goal, URL:String, cell:GoalTableViewCell){
        
        Alamofire.request(URL + goal.image).responseImage { response in
            cell.goalImage.image = response.result.value
        }
    }
    
}

