//
//  GoalViewController.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 13/02/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import UIKit
import Alamofire

class TasksTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameTaskCell: UILabel!
    @IBOutlet weak var dateRealizationTaskCell: UILabel!
    @IBOutlet weak var removeButtonTaskCell: UIButton!
    @IBOutlet weak var statusButtonTaskCell: UIButton!
    @IBOutlet weak var calendarTaskCell: UIImageView!

    
}

class GoalViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    
    var passedImage:UIImageView!
    var passedValues:[String]!
    var valuesGoalToPass = [String]()
    var tasks = [Task]()
    let URL = "https://secure-plains-94243.herokuapp.com/api/tasks.json"
    
    @IBOutlet weak var goalImage: UIImageView!
    @IBOutlet weak var goalName: UILabel!
    @IBOutlet weak var goalDate: UILabel!
    @IBOutlet weak var tasksTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSlideMenuButton()
        self.title = "Objetivo"
        getTasks()
        tasksTableView.dataSource = self
        tasksTableView.delegate = self
        goalImage.image = passedImage?.image
        goalName.text = passedValues[0]
        goalDate.text = passedValues[1]
        valuesGoalToPass.append(goalName.text!)
        valuesGoalToPass.append(goalDate.text!)

        
    }
    @IBAction func newTask(_ sender: UIButton) {
        self.performSegue(withIdentifier: "newTaskSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "newTaskSegue") {
            
            let viewController = segue.destination as! NewTaskViewController
            if valuesGoalToPass.count > 0 {
                viewController.valuesGoalReceived = valuesGoalToPass
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellTasks", for: indexPath) as! TasksTableViewCell
        let task = self.tasks[indexPath.row]
        cell.nameTaskCell.text = task.descricao
        cell.dateRealizationTaskCell.text = task.dateRealization
        
        if task.status == "finalizado" {
            cell.statusButtonTaskCell.setBackgroundImage(UIImage(named: "checkmark.png"), for: UIControlState.normal)
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let btn = UIButton(type: UIButtonType.custom)
//        btn.tag = indexPath.row
//        self.onCloseMenuClick(btn)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Tarefas"
    }
    
    func getTasks(){
        
        Alamofire.request(URL).responseArray { (response: DataResponse<[Task]>) in
            
            let statusCode = ((response.response?.statusCode))!
            
            if statusCode == 200 {
                self.tasks.append(contentsOf: response.result.value!)
                self.tasksTableView.reloadData()
            }else
            {
                AlertUtil.showAlert(title: "Falha",message: "Nao foi possivel carregar as tarefas", context: self)
            }
        }
    }

}
