//
//  Session.swift
//  PocketCoatch
//
//  Created by Rafael Aragão Câmara on 13/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import ObjectMapper

class Session: Object {
    
    var id = ""
    var email = ""
    var password = ""
    var success = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Session : Mappable {
    func mapping(map: Map){
        id <- map["id"]
        email <- map["email"]
        password <- map["password"]
        success <- map["success"]
        
    }
}

extension Session: RealmGenericDAO {
    
    static func save(object: Session) {
        let realm = try! Realm()
        do {
            try realm.write{
                realm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o jogo")
        }
    }
    
    static func saveAll(objects: [Session]) {
        let realm = try! Realm()
        do {
            try realm.write {
                realm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os jogos")
        }
    }
    
    static func getByID(id: AnyObject) -> Session? {
        let realm = try! Realm()
        
        let results = realm.objects(Session.self).filter("id = %@", id)
        return results.first
    }
    
    static func all() -> [Session]? {
        let realm = try! Realm()
        
        var list: [Session] = []
        list.append(contentsOf: realm.objects(Session.self))
        return list
    }
    
}



