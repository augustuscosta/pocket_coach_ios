//
//  Validate.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 27/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation

import UIKit

class Validate {
    
    
    static func check(field: String, min: Int , max: Int , name: String, context: UIViewController) -> Bool {
        var result = false
        if field.characters.count != 0 {
            
            switch name {
            case "Email":
                result = checkEmail(text: field, name: name, context: context)
            case "CPF/CNPJ":
                result = checkCpfCnpj(text: field, min: min , max: max , name: name, context: context)
                break
            default:
                result = checkField(text: field, min: min, max: max, name: name, context: context)
            }
            
        } else {
            AlertUtil.showAlert(title: "Campo obrigatório", message: "Campo \(name) não preenchido!", context: context)
        }
        
        return result
    }
    
    
    static func checkEmail (text: String, name: String, context: UIViewController) -> Bool {
        if isValidEmail(text: text) {
            return true
        } else {
            AlertUtil.showAlert(title: "Campo Inválido", message: "Email inválido!", context: context)
            return false
        }
    }
    
    static func isValidEmail(text: String) -> Bool {
        let email = NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}")
        return email.evaluate(with: text)
    }
    
    
    static func checkCpfCnpj(text: String, min: Int , max: Int , name: String, context: UIViewController) -> Bool {
        
        let size = text.characters.count
        if size != max && size != min {
            AlertUtil.showAlert(title: "\(name) inválido", message: "O campo \(name) deve ter entre 10 e 11 caracteres!!", context: context)
            return false
        }
        return true
    }
    
    
    static func checkField(text: String, min: Int , max: Int , name: String, context: UIViewController) -> Bool {
        let size = text.characters.count
        if (size >= min && size <= max) {
            return  true;
        } else {
            catchError(name: name, context: context)
            return false
        }
        
    }
    
    static func catchError(name: String, context: UIViewController) {
        if name == "Senha" {
            AlertUtil.showAlert(title: "Senha Inválida", message: "A senha deve ter no mínimo 6 caracteres!", context: context)
        } else if name == "Telefone" {
            AlertUtil.showAlert(title: "Telefone Inválido", message: "O campo Telefone deve ter entre 10 e 11 caracteres!", context: context)
        } else {
            AlertUtil.showAlert(title: "Campo Inválido", message: "Campo \(name) inválido!", context: context)
        }
    }
    
    static func checkConfirmPass(pass: String, confirmPass: String, name: String , context: UIViewController) -> Bool {
        if pass != "" && confirmPass != "" {
            
            if pass == confirmPass {
                return true
            } else {
                AlertUtil.showAlert(title: "Senhas Diferentes", message: "As sennhas devem ser idênticas!", context: context)
                return false
            }
            
        } else {
            AlertUtil.showAlert(title: "Campo Obrigatório", message: "Campo \(name) não preenchido!", context: context)
            return false
        }
    }
    
    static func checkImage(image: UIImage?, name: String, context: UIViewController) -> Bool {
        
        var result = false
        
        if image != nil {
            result = true
        } else {
            AlertUtil.showAlert(title: "Campo Obrigatório", message: "Campo \(name) não preenchido!", context: context)
        }
        return result
        
    }
    
    static func checkIfAllAreTrue(list: [Bool]) -> Bool {
        for item in list {
            if !item {
                return false
            }
        }
        return true
    }
    
    
}
