//
//  AlertUtil.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 27/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation

import UIKit

class AlertUtil{
    
    static func showAlert(title:String, message:String, context: UIViewController){
        
        let alert = callAlert(title: title, message: message)
        context.present(alert,animated: true, completion:nil)
        
    }
    
    static func showAlertPhotolibraryOrCamera(title:String, message:String, context: NewGoalViewController){

        let alert = callAlertPhotolibraryOrCamera(title: title, message: message, context: context)
        context.present(alert,animated: true, completion:nil)
        
    }
    
    static func callAlert(title:String, message:String) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        return alertController
    }
    
    static func callAlertPhotolibraryOrCamera(title:String, message:String, context:NewGoalViewController) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let imageObtained = UIImagePickerController()
        imageObtained.delegate = context
        
        let PhotolibraryAction = UIAlertAction(title: "Photolibrary", style: .default) { (action:UIAlertAction!) in
            imageObtained.sourceType = UIImagePickerControllerSourceType.photoLibrary
            context.present(imageObtained, animated: true, completion: nil)
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action:UIAlertAction!) in
            imageObtained.sourceType = UIImagePickerControllerSourceType.camera
            context.present(imageObtained, animated: true, completion: nil)
        }
        
        alertController.addAction(PhotolibraryAction)
        alertController.addAction(cameraAction)
        return alertController
    }
    
}
