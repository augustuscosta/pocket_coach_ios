//
//  Util.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit

class Util {
    
//    https://pocketcoach1.herokuapp.com/api/
//    http://pocketcoach1.com.br/api/
    
    static let rootUrl = "https://pocketcoach1.herokuapp.com/api/"
    static let root = "https://pocketcoach1.herokuapp.com/"
    
    static let formatter = DateFormatter()
    
    static func getView(storyboard: String, viewController: String) -> UIViewController {
        return UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    static func showView(name: String, context: UIViewController) {
        context.present(Util.getView(storyboard: "Main", viewController: name), animated: true, completion: nil)
    }
    
    static func alertNoInternet(context: UIViewController){
        Util.showAlert(alert: Util.getAlert(title: "Erro na Conexão", message: "Sem conexão com a internet!"), context: context)
    }
    
    static func showAlert(alert: UIAlertController, context: UIViewController){
        context.present(alert, animated: true, completion: nil)
    }
    
    static func getAlert(title: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
        }
        alert.addAction(cancelAction)
        
        return alert
    }
    
    static func getShadow(view: UIView) {
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowOpacity = 1.0
        view.layer.masksToBounds = false
        view.layer.shadowRadius = 10
    }
    
    static func alertNotAllowed() -> UIAlertController {
        
        let alert = UIAlertController(title: "Permissão", message: "Este app precisa de permissão para usar a funcionalidade desejada!",preferredStyle: .alert)
        
        let openConfig = UIAlertAction(title: "Abrir Configurações", style: .default, handler: { (alertConfig) in
            if let config = NSURL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(config as URL)
            }
        })
        
        let cancel = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
        
        alert.addAction(openConfig)
        alert.addAction(cancel)
        
        return alert
    }
    
    static func alertNotFound(context: UIViewController) -> UIAlertController {
        
        let alert = UIAlertController(title: "Nenhum Atendimento Encontrado", message: "Deseja realiza uma nova busca?",preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (result) in
            self.showView(name: "NewAttendenceViewController", context: context)
        })
        
        alert.addAction(ok)
        
        return alert
    }
    
    static func getDateString(date: Date) -> String {
        self.formatter.dateFormat = "dd/MM/yyyy - HH:mm"
        self.formatter.timeZone = TimeZone(identifier: "UTC")
        return self.formatter.string(from: date)
    }
    
    static func getDate(dateStr: String) -> Date? {
        self.formatter.timeZone = TimeZone(identifier: "UTC")
        self.formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return self.formatter.date(from: dateStr)
    }
    
    static func getOnlyDate(date: Date) -> String {
        self.formatter.dateFormat = "dd/MM/yyyy"
        self.formatter.timeZone = TimeZone(identifier: "UTC")
        return self.formatter.string(from: date)
    }
    
    static func getOnlyHours(date: Date) -> String {
        self.formatter.dateFormat = "HH:mm"
        self.formatter.timeZone = TimeZone(identifier: "UTC")
        return self.formatter.string(from: date)
    }
}
