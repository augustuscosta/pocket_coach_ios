//
//  DropDownUtil.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 23/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation
import UIKit
import DropDown

class DropDownUtil {
    
    static func getDropDown(dropDown:DropDown, anchor:UIView, options:[String],dropDownButton:UIButton,context:UIViewController,user:User?, campo:String) -> DropDown?{
        dropDown.anchorView = anchor
        dropDown.dataSource = options
        dropDown.selectionAction = {(index: Int, item: String) in
//            print("Selected item: \(item) at index: \(index)")
            dropDownButton.setTitle(item, for: UIControlState.normal)
            if user != nil{
                switch campo {
                    
                case "sex":
                    user?.gender = item
                    break
                case "schooling":
                    user?.schooling = item
                    break
                case "icome":
                    user?.rent = item
                    break
                case "religion":
                    user?.religion = item
                    break
                case "affectiveSituation":
                    user?.affectiveSituation = item
                    break
                case "children":
                    user?.child = item
                    break
                case "employmentSituation":
                    user?.employmentSituation = item
                    break
                default: break
                    
                }
            }
            
        }
        
        return dropDown
    }
    
    static func chooseRepeatDropDownAction(dropDown:DropDown,dropDownButton:UIButton,views:[UIView]) -> DropDown?{
        
        dropDown.selectionAction = {(index: Int, item: String) in
            dropDownButton.setTitle(item, for: UIControlState.normal)
            showRepeatView(repeatChoose:item,sender: dropDownButton,vews: views)
        }
        return dropDown
    }


    static func showRepeatView(repeatChoose:String, sender:UIButton, vews:[UIView]){
        switch sender.title(for: UIControlState.normal)! {
        case "Semanal":
            vews[0].isHidden = true
            vews[1].isHidden = true
            vews[2].isHidden = false
            
            break
        case "Mensal":
            vews[0].isHidden = true
            vews[1].isHidden = false
            vews[2].isHidden = true
            break
            
        case "Uma vez":
            vews[0].isHidden = false
            vews[1].isHidden = true
            vews[2].isHidden = true
            break
            
        default:
            break
        }
    }
}
