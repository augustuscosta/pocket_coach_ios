//
//  RealmGenericDAO.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 20/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import Foundation

protocol RealmGenericDAO {
    
    associatedtype T
    
    static func save(object: T)
    
    static func saveAll(objects: [T])
    
    static func all() -> [T]?
    
    static func getByID(id: AnyObject) -> T?
}
