//
//  Request.swift
//  medko_ios
//
//  Created by Thialyson Martins on 02/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import AlamofireObjectMapper

class Request {
    
//    static var avatar = ""
//    static var nameCurrentUser = ""
//    
//    static var specialityNameList = [String]()
//    static var specialityNameListHomecare = [String]()
//    static var examTypeNameList = [String]()
//    static var examTypeNameListHomeCare = [String]()
//    static var attendeceTypeNameList = [String]()
//    static var paymentMethodNameList = [String]()
//    static var healthPlanNameList = [String]()
//    
////    static var attendenceList = [Attendence]()
////    static var specialiyList = [Speciality]()
////    static var examTypeList = [ExamType]()
////    static var healthPlanList = [HealthPlan]()
////    static var paymentMethodList = [PaymentMethod]()
////    static var attendenceTypeList = [AttendenceType]()
//    
//    
//    
//    static func login(url: String, params: [String: Any], paramsFb: [String: [String: Any]], context: UIViewController) {
//        
//        let parameters = params.isEmpty ? paramsFb : params
//        Alamofire.request(url, method: .post, parameters: parameters)
//            .responseObject { (response: DataResponse<Session>) in
//            switch response.result {
//            case .failure:
//                Util.alertNoInternet(context: context)
//            case .success:
//                if response.result.value?.success == true {
//                    Session.save(object: response.result.value!)
//                    Request.requestCurrentUser(context: context)
//                } else {
//                    Util.showAlert(alert: Util.getAlert(title: "Erro ao Logar", message: "Usuário ou Senha incorreto!"), context: context)
//                }
//            }
//        }
//    }
//    
//    static func requestSpecialities(context: UIViewController) {
//        
//        var nameList = [String]()
//        let url = "\(Util.rootUrl)/specialities"
//        self.specialityNameListHomecare.removeAll()
//        
//        Alamofire.request(url).responseArray { (response: DataResponse<[Speciality]>) in
//            switch response.result {
//            case .failure:
//                Util.alertNoInternet(context: context)
//            case .success:
//                if let specialityObj = response.result.value {
//                    Speciality.saveAll(objects: specialityObj)
//                    
//                    if let list = Speciality.getAll() {
//                        specialiyList.append(contentsOf: list)
//                        for item in list {
//                            if item.homeCare == true {
//                                self.specialityNameListHomecare.append(item.name)
//                            }
//                            nameList.append(item.name)
//                        }
//                        self.specialityNameList = nameList
//                    }
//                } else {
//                    print("Falha na requisição Specialidades")
//                }
//            }
//        }
//    }
//    
//    static func requestExamTypes(context: UIViewController) {
//        var nameList = [String]()
//        let url = "\(Util.rootUrl)/exam_types"
//        self.examTypeNameListHomeCare.removeAll()
//        
//        Alamofire.request(url).responseArray { (response: DataResponse<[ExamType]>) in
//            switch response.result {
//            case .failure:
//                Util.alertNoInternet(context: context)
//            case .success:
//                if let examTypeObj = response.result.value {
//                    ExamType.saveAll(objects: examTypeObj)
//                    if let list = ExamType.getAll() {
//                        examTypeList.append(contentsOf: list)
//                        for item in list {
//                            if item.homeCare == true {
//                                self.examTypeNameListHomeCare.append(item.name)
//                            }
//                            nameList.append(item.name)
//                        }
//                        self.examTypeNameList = nameList
//                    }
//                } else {
//                    print("Falha na requisição ExamType")
//                }
//            }
//        }
//    }
//    
//    static func register(parameters: [String : String], idsSpecialities: [String]? ,idsExams: [String]?, avatar: UIImage?, crm: UIImage?, proofOfAddress: UIImage?, context: UIViewController) {
//        
//        Alamofire.upload(multipartFormData: { data in
//            
//            if !parameters.isEmpty{
//                for (key, value) in parameters {
//                    data.append(value.data(using: String.Encoding.utf8)!, withName: key)
//                }
//            }
//            
//            if !(idsSpecialities?.isEmpty)! {
//                for index in 0..<idsSpecialities!.count {
//                    data.append(idsSpecialities![index].data(using: String.Encoding.utf8)!, withName: "user[speciality_ids][]" )
//                }
//            }
//            
//            if !(idsExams?.isEmpty)!{
//                for index in 0..<idsExams!.count {
//                    data.append(idsSpecialities![index].data(using: String.Encoding.utf8)!, withName: "user[exam_type_ids][]" )
//                }
//            }
//            
//            if avatar != nil {
//                data.append(UIImageJPEGRepresentation(avatar!, 1)!, withName: "user[avatar]", fileName: "avatar.jpeg", mimeType: "image/jpeg")
//            }
//            
//            if crm != nil {
//                data.append(UIImageJPEGRepresentation(crm!, 1)!, withName: "user[crm_image]", fileName: "crm.jpeg", mimeType: "image/jpeg")
//            }
//            
//            if proofOfAddress != nil {
//                data.append(UIImageJPEGRepresentation(proofOfAddress!, 1)!, withName: "user[proof_of_address_image]", fileName: "proof_of_address_image.jpeg", mimeType: "image/jpeg")
//            }
//            
//        }, to: "\(Util.rootUrl)/users/register_mobile_multipart") { (result) in
//            
//            switch result {
//            case .success(let upload, _, _):
//                
//                upload.responseObject { (response: DataResponse<Session>) in
//                    if response.result.isSuccess{
//                        
//                        if response.result.value?.success == true {
//                            
//                            if (parameters["user[active]"] != "true") {
//                                Util.showAlert(alert: Util.getAlert(title: "Validar Conta", message: "Ainda não é possivel realizar o seu acesso. A administração do Medko vai validar a sua conta!"), context: context)
//                                
//                            } else {
//                                Util.showView(name: "AttendenceListViewController", context: context)
//                                
//                            }
//                        } else {
//                            Util.showAlert(alert: Util.getAlert(title: "Erro no Cadastro", message: "Erro ao tentar fazer o cadastro!"), context: context)
//                        }
//                        
//                    }
//                    Session.save(object: response.result.value!)
//                }
//                
//            case .failure(let encodingError):
//                print(encodingError)
//                break
//            }
//        }
//    }
//    
//    static func requestCurrentUser(context: UIViewController) {
//        let url = "\(Util.rootUrl)/current_user.json"
//        Alamofire.request(url).responseObject { (response: DataResponse<User>) in
//            switch response.result {
//            case .failure:
//                Util.alertNoInternet(context: context)
//            case .success:
//                if let userObj = response.result.value {
//                    avatar = userObj.avatar
//                    nameCurrentUser = userObj.name
//                    UserDefaults.standard.set(userObj.accountTypeId, forKey: "accountTypeId")
//                    userObj.currentUser = true
//                    User.deleteUsers()
//                    User.save(object: userObj)
//                    Util.showView(name: "AttendenceListViewController", context: context)
//                } else {
//                    print("Falha na requisição Current User")
//                }
//            }
//        }
//    }
//    
//    static func getAttendeces(handler: @escaping ([Attendence]?, Error?) -> ()) {
//        
//        Alamofire.request("\(Util.rootUrl)/attendences.json")
//            .responseArray { (response: DataResponse<[Attendence]>) in
//                switch response.result {
//                case .success(let value):
//                    Attendence.saveAll(objects: value)
//                    AttendenceListViewController.attendenceList = value
//                    handler(value, nil)
//                case .failure(let error):
//                    handler(nil, error)
//                }
//        }
//    }
//    
//    static func getAttendece(id: Int, handler: @escaping (Attendence?, Error?) -> ()) {
//        Alamofire.request("\(Util.rootUrl)attendences/\(id).json")
//            .responseObject { (response: DataResponse<Attendence>) in
//                switch response.result {
//                case .success(let value):
//                    Attendence.save(object: value)
//                    handler(value, nil)
//                case .failure(let error):
//                    handler(nil, error)
//                }
//        }
//    }
//    
//    
//    static func requestAttendenceType(context: UIViewController) {
//        var nameList = [String]()
//        let url = "\(Util.rootUrl)/attendence_types.json"
//        
//        Alamofire.request(url).responseArray { (response: DataResponse<[AttendenceType]>) in
//            switch response.result {
//            case .failure:
//                Util.alertNoInternet(context: context)
//            case .success:
//                if let objList = response.result.value {
//                    AttendenceType.saveAll(objects: objList)
//                    if let list = AttendenceType.getAll() {
//                        attendenceTypeList.append(contentsOf: list)
//                        for item in list {
//                            nameList.append(item.name)
//                        }
//                        self.attendeceTypeNameList = nameList
//                    }
//                    
//                } else {
//                    print("Falha na requisição ExamType")
//                }
//            }
//        }
//    }
//    
//    static func requestPaymentMethod(context: UIViewController) {
//        var nameList = [String]()
//        let url = "\(Util.rootUrl)/payment_methods.json"
//        
//        Alamofire.request(url).responseArray { (response: DataResponse<[PaymentMethod]>) in
//            switch response.result {
//            case .failure:
//                Util.alertNoInternet(context: context)
//            case .success:
//                if let objList = response.result.value {
//                    PaymentMethod.saveAll(objects: objList)
//                    if let list = PaymentMethod.getAll() {
//                        paymentMethodList.append(contentsOf: list)
//                        for item in list {
//                            nameList.append(item.name)
//                        }
//                        self.paymentMethodNameList = nameList
//                    }
//                }
//            }
//        }
//    }
//    
//    static func requestHelathPlan(context: UIViewController) {
//        var nameList = [String]()
//        let url = "\(Util.rootUrl)/health_plans.json"
//        
//        Alamofire.request(url).responseArray { (response: DataResponse<[HealthPlan]>) in
//            switch response.result {
//            case .failure:
//                Util.alertNoInternet(context: context)
//            case .success:
//                if let objList = response.result.value {
//                    HealthPlan.saveAll(objects: objList)
//                    if let list = HealthPlan.getAll() {
//                        healthPlanList.append(contentsOf: list)
//                        for item in list {
//                            nameList.append(item.name)
//                        }
//                        self.healthPlanNameList = nameList
//                    }
//                }
//            }
//        }
//    }
//    
//    
//    static func getAttendeceHomeCare(searchParameters: [String: String], handler: @escaping (Attendence?, Error?) -> ()) {
//        let params = searchParameters
//        let url = "\(Util.rootUrl)attendences/search_attendence_home_care.json"
//        
//        Alamofire.request(url, method: .post, parameters: params)
//            .responseObject { (response: DataResponse<Attendence>) in
//                switch response.result {
//                case .success(let value):
//                    handler(value, nil)
//                case .failure(let error):
//                    print(error)
//                    handler(nil, error)
//                }
//        }
//    }
//    
//    static func changeAttendenceStatus(url: String, params: [String: Any], handler: @escaping (Bool , Error?) -> ()) {
//        Alamofire.request(url, method: .post, parameters: params)
//            .responseJSON { (response: DataResponse<Any>) in
//            switch response.result {
//            case .success:
//                handler(true, nil)
//            case .failure(let error):
//                handler(false, error)
//            }
//        }
//    }
//    
//    static func getServiceWindowsList(searchParameters: [String: String], handler: @escaping ([ServiceWindows]?, Error?) -> ()) {
//        let params = searchParameters
//        let url = "\(Util.rootUrl)attendences/search_attendence_clinic.json"
//        
//        Alamofire.request(url, method: .post, parameters: params)
//            .responseArray { (response: DataResponse<[ServiceWindows]>) in
//                switch response.result {
//                case .success(let value):
//                    ServiceWindows.saveAll(objects: value)
//                    handler(value, nil)
//                case .failure(let error):
//                    handler(nil, error)
//                }
//        }
//    }
//    
//    static func createAttendence(params: [String: Any], handler: @escaping (Attendence?, Error?) -> ()) {
//        let params = params
//        let url = "\(Util.rootUrl)attendences/create_attendence.json"
//        
//        Alamofire.request(url, method: .post, parameters: params)
//            .responseObject { (response: DataResponse<Attendence>) in
//                switch response.result {
//                case .success(let value):
//                    Attendence.save(object: value)
//                    handler(value, nil)
//                case .failure(let error):
//                    handler(nil, error)
//                }
//        }
//    }
//    
//    static func requestPayment(id: Int, handler: @escaping (PaymentRequest?, Error?) -> ()) {
//        let url = "\(Util.rootUrl)attendences/payment/\(id).json"
//        Alamofire.request(url).responseObject { (response: DataResponse<PaymentRequest>) in
//                switch response.result {
//                case .success(let value):
//                    print(value)
//                    handler(value, nil)
//                case .failure(let error):
//                    print(error)
//                    handler(nil, error)
//                }
//        }
//    }
//    
//    static func changeLocation(params: [String: Any], context: UIViewController) {
//        let url = "\(Util.rootUrl)/points.json"
//        Alamofire.request(url, method: .post, parameters: params)
//            .responseJSON { (response: DataResponse<Any>) in
//            switch response.result {
//            case .success(let value):
//                print(value)
//            case .failure:
//                Util.alertNoInternet(context: context)
//            }
//        }
//    }
//    
//    static func logout(context: UIViewController, handler: @escaping (Bool?, Error?) -> ()) {
//        let url = "\(Util.rootUrl)/users/sign_out_mobile"
//        Alamofire.request(url, method: .delete)
//            .responseJSON { (response: DataResponse<Any>) in
//                switch response.result {
//                case .success:
//                    handler(true, nil)
//                case .failure(let error):
//                    handler(nil, error)
//                    Util.alertNoInternet(context: context)
//                }
//        }
//    }
}
