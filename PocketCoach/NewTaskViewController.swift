//
//  NewTaskViewController.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 15/02/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import UIKit
import DropDown
import M13Checkbox

class NewTaskViewController: BaseViewController {
    
    var valuesGoalReceived:[String]!
    
    var dropDownRepeat = DropDown()
    var repeatOptions = ["Uma vez","Mensal", "Semanal"]
    
    @IBOutlet weak var repeatView: UIView!
    @IBOutlet weak var repeatButton: UIButton!
    @IBOutlet weak var goalNameLabel: UILabel!
    @IBOutlet weak var goalDateLabel: UILabel!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var viewSanday: UIView!
    @IBOutlet weak var viewMonday: UIView!
    @IBOutlet weak var viewTuesday: UIView!
    @IBOutlet weak var viewWednesday: UIView!
    @IBOutlet weak var viewThursday: UIView!
    @IBOutlet weak var viewFriday: UIView!
    @IBOutlet weak var viewSaturday: UIView!
    @IBOutlet weak var viewAllDays: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSlideMenuButton()
        self.title = "Nova tarefa"
        goalNameLabel.text = valuesGoalReceived[0]
        goalDateLabel.text = valuesGoalReceived[1]
        dropDownRepeat = DropDownUtil.getDropDown(dropDown:dropDownRepeat, anchor: self.repeatView,options: repeatOptions,dropDownButton: self.repeatButton, context:self,user: nil,campo: "")!
        let checkbox = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0))
            viewSanday.addSubview(checkbox)
        let checkboxSeg = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0))
        viewMonday.addSubview(checkboxSeg)
        let checkboxTer = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0))
        viewTuesday.addSubview(checkboxTer)
        let checkboxQua = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0))
        viewWednesday.addSubview(checkboxQua)
        let checkboxQui = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0))
        viewThursday.addSubview(checkboxQui)
        let checkboxSex = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0))
        viewFriday.addSubview(checkboxSex)
        let checkboxSab = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0))
        viewSaturday.addSubview(checkboxSab)
        let checkboxAll = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0))
        viewAllDays.addSubview(checkboxAll)
        
        
    }
    
    @IBAction func chooseRepeatDropDown(_ sender: UIButton) {
        
        DropDownUtil.chooseRepeatDropDownAction(dropDown: dropDownRepeat, dropDownButton: sender, views: [view1,view2,view3])?.show()
        
    }
    
}
