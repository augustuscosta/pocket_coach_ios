//
//  RegisterViewController.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 17/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import UIKit
import DropDown
import Alamofire

class RegisterViewController: UIViewController {
    
    var dropDownSex = DropDown()
    var dropDownSchooling = DropDown()
    var dropDownIcome = DropDown()
    var dropDownReligion = DropDown()
    var dropDownAffectiveSituation = DropDown()
    var dropDownChildren = DropDown()
    var dropDownEmploymentSituation = DropDown()
    var dropDownMaritalStatus = DropDown()
    
    
    var sexOptions = ["Masculino","Femenino"]
    var schoolingOptions =
        ["Fundamental - Incompleto",
         "Fundamental - Completo",
         "Médio - Incompleto",
         "Médio - Completo",
         "Superior - Incompleto",
         "Superior - Completo",
         "Pós-graduação - Incompleto",
         "Pós-graduação - Completo",
         "Mestrado - Incompleto",
         "Mestrado - Completo",
         "Doutorado - Incompleto",
         "Doutorado - Completo"]
    var icomeOptions = ["Alta","Media","Baixa"]
    var religionOptions = ["Catolico","Evangelico","Espirita","Budista","Islamista","Judeu","Umbandista","Agnostico","Ateu"]
    var maritalStatusOptions = ["Solteiro", "Casado", "Divorciado", "União estavél", "Viuvo"]
    var affectiveSituationOptions = ["Solteiro", "Namorando", "Enrolado"]
    var childrenOptions = ["Sim", "Não"]
    var employmentSituationOptions = ["Empregado", "Desempregado"]
    var user = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropDownSex = DropDownUtil.getDropDown(dropDown:dropDownSex, anchor: self.sexField,options: sexOptions,dropDownButton: self.sexButton, context:self,user: user,campo: "sex")!
        
        dropDownSchooling = DropDownUtil.getDropDown(dropDown:dropDownSchooling, anchor: self.schoolingField,options: schoolingOptions,dropDownButton: self.schoolingButton, context:self,user: user,campo: "schooling")!
        
        dropDownIcome = DropDownUtil.getDropDown(dropDown:dropDownIcome, anchor: self.icomeField,options: icomeOptions,dropDownButton: self.icomeButton, context:self,user: user,campo: "icome")!
        
        dropDownReligion = DropDownUtil.getDropDown(dropDown:dropDownReligion, anchor: self.religionField,options: religionOptions,dropDownButton: self.religionButton, context:self,user: user,campo: "religion")!
        
        dropDownAffectiveSituation = DropDownUtil.getDropDown(dropDown:dropDownAffectiveSituation, anchor: self.affectiveSituationField,options: affectiveSituationOptions,dropDownButton: self.affectiveSituationButton, context:self,user: user,campo: "affectiveSituation")!
        
        dropDownChildren = DropDownUtil.getDropDown(dropDown:dropDownChildren, anchor: self.childrenField,options: childrenOptions,dropDownButton: self.childrenButton, context:self, user: user,campo: "children")!
        
        dropDownEmploymentSituation = DropDownUtil.getDropDown(dropDown:dropDownEmploymentSituation, anchor: self.employmentSituationField,options: employmentSituationOptions,dropDownButton: self.employmentSituationButton, context:self, user: user,campo: "employmentSituation")!
        
        dropDownMaritalStatus = DropDownUtil.getDropDown(dropDown:dropDownMaritalStatus, anchor: self.maritalStatusField,options: maritalStatusOptions,dropDownButton: self.maritalStatusButton, context:self, user: user,campo: "maritalStatus")!
        scrollView.contentSize.height = 644
    }
    
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var sexField: UIView!
    @IBOutlet weak var schoolingField: UIView!
    @IBOutlet weak var icomeField: UIView!
    @IBOutlet weak var religionField: UIView!
    @IBOutlet weak var birthField: UITextField!
    @IBOutlet weak var affectiveSituationField: UIView!
    @IBOutlet weak var childrenField: UIView!
    @IBOutlet weak var employmentSituationField: UIView!
    @IBOutlet weak var maritalStatusField: UIView!
    
    @IBAction func registerButton(_ sender: Any) {
        var userWrapper:User = User()
        userWrapper = setUser(user: self.user)
        print(userWrapper.affectiveSituation)
        
        let parameters = [
            "user":[
                "email": userWrapper.email,
                "password": userWrapper.password,
                "name": userWrapper.name,
                "child":userWrapper.child,
                "gender":userWrapper.gender,
                "dateOfBirth": userWrapper.dateOfBirth,
                "schooling": userWrapper.schooling,
                "rent": userWrapper.rent,
                "religion": userWrapper.religion,
                "affective_situation": userWrapper.affectiveSituation,
                "employment_situation": userWrapper.employmentSituation,
            ]
        ]
        
        var statusCode = Int()
        
        Alamofire.request("https://secure-plains-94243.herokuapp.com/api/users/register_mobile", method: .post, parameters: parameters).responseObject {(response:DataResponse<Session>) in
            
            statusCode = (response.response?.statusCode)!
            
            if statusCode == 200 && response.result.value?.success == true{
                AlertUtil.showAlert(title: "Sucesso",message: "Usuario cadastrado com sucesso!", context: self)
            }else{
                AlertUtil.showAlert(title: "Falha",message: "Erro ao cadastrar o usuario!", context: self)
            }
        }
    }
    
    @IBOutlet weak var sexButton: UIButton!
    @IBAction func sexButton(_ sender: Any) {
        dropDownSex.show()
    }
    
    @IBOutlet weak var schoolingButton: UIButton!
    @IBAction func schoolingButton(_ sender: Any) {
        dropDownSchooling.show()
    }
    
    @IBOutlet weak var icomeButton: UIButton!
    @IBAction func icomeButton(_ sender: Any) {
        dropDownIcome.show()
    }
    
    @IBOutlet weak var religionButton: UIButton!
    @IBAction func religionButton(_ sender: Any) {
        dropDownReligion.show()
    }
    
    @IBOutlet weak var maritalStatusButton: UIButton!
    @IBAction func maritalStatusButton(_ sender: Any) {
        dropDownMaritalStatus.show()
    }
    
    @IBOutlet weak var affectiveSituationButton: UIButton!
    @IBAction func affectiveSituationButton(_ sender: Any) {
        dropDownAffectiveSituation.show()
    }
    @IBOutlet weak var childrenButton: UIButton!
    @IBAction func childrenButton(_ sender: Any) {
        dropDownChildren.show()
    }
    
    @IBOutlet weak var employmentSituationButton: UIButton!
    @IBAction func employmentSituationButton(_ sender: Any) {
        dropDownEmploymentSituation.show()
    }
    
    func setUser(user:User) -> User {
        user.name = nameField.text!
        user.email = mailField.text!
        user.password = passwordField.text!
        user.dateOfBirth = birthField.text!
        return user
    }
    
    
    
}

