//
//  ViewController.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 17/01/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import FacebookLogin
import FacebookCore

class ViewController: UIViewController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    var success:Bool!
    @IBOutlet weak var userMail: UITextField!
    
    @IBOutlet weak var userPassword: UITextField!
    
    @IBAction func login(_ sender: Any) {
        
        userMail.text = "paulo@gmail.com"
        userPassword.text = "123456"
        
        if ((userMail.text?.isEmpty)! && (userPassword.text?.isEmpty)!) {
            AlertUtil.showAlert(title: "Campo Vazio",message: "O email e senha são obrigatorios", context: self)
        }
        
        var email = ""
        email = userMail.text!
        var password = ""
        password = userPassword.text!
        
        let parameters = [
            "email": email,
            "password": password
        ]
        var statusCode = Int()
        
        Alamofire.request(Util.rootUrl+"users/sign_in_mobile", method: .post, parameters: parameters).responseObject {(response:DataResponse<Session>) in
            
            statusCode = ((response.response?.statusCode))!
            
            if statusCode == 200 && response.result.value?.success == true{
                self.callNextViewControle(identifier: "HomeViewController")
            }else{
                AlertUtil.showAlert(title: "Falha no Login",message: "Usuario ou senha invalido", context: self)
            }
        }
        
    }
    
    @IBAction func register(_ sender: Any) {
        self.callNextViewControle(identifier: "RegisterViewController")
    }
    
    @IBAction func loginWithFacebook(_ sender: Any) {
        
        LoginManager().logIn([.publicProfile, .email], viewController: self) { loginResult in
            switch loginResult {
                
                
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, let accessToken):
                print("token: ", accessToken.authenticationToken)
                self.getParmsFacebook(token: accessToken.authenticationToken)
                self.callNextViewControle(identifier: "RegisterViewController")
            }
        }
    }
    
    func getParmsFacebook(token: String){
        let params = ["fields" : "email, name"]
        let graphRequest = GraphRequest(graphPath: "me", parameters: params)
        graphRequest.start {
            (urlResponse, requestResult) in
            
            switch requestResult {
            case .failed(let error):
                print("error in graph request:", error)
                break
            case .success(let graphResponse):
                if let responseDictionary = graphResponse.dictionaryValue {
                    print(responseDictionary)
                    
                    print(responseDictionary["name"] as Any)
                    print(responseDictionary["email"] as Any)
                }
            }
        }
    }
    
    func callNextViewControle(identifier: String){
        
        let storeBoard = UIStoryboard(name: "Main", bundle:nil)
        let controler = storeBoard.instantiateViewController(withIdentifier: identifier)
        self.present(controler, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    
    
}
