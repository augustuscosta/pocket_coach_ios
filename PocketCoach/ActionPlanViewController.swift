//
//  ActionPlanViewController.swift
//  PocketCoach
//
//  Created by Rafael Aragão Câmara on 21/02/17.
//  Copyright © 2017 Rafael Aragão Câmara. All rights reserved.
//

import UIKit
import Alamofire
import M13Checkbox

class ActionPlanTaskTableViewCell: UITableViewCell{
    
    @IBOutlet weak var finalizationTaskView: UIView!
    @IBOutlet weak var proibitionButton: UIButton!
    @IBOutlet weak var excludeTaskButton: UIButton!
    @IBOutlet weak var taskNameLabel: UILabel!
    
}

class ActionPlanViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    var sections = [String]()
    var items = [[Task]]()
    var tasks = [Task]()
    let URL = "https://secure-plains-94243.herokuapp.com/api/tasks.json"
    
    @IBOutlet weak var actionPlanTaskTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSlideMenuButton()
        self.title = "Plano de ação"
        actionPlanTaskTableView.dataSource = self
        actionPlanTaskTableView.delegate = self
        loadSessions()
    }
    
    @IBAction func cancelTask(_ sender: UIButton) {
        print("Tarefa cancelada")
    }
    
    @IBAction func deleteTask(_ sender: UIButton) {
        print("Tarefa excluida")
    }
    
    func loadSessions(){
        
        Alamofire.request(URL).responseArray { (response: DataResponse<[Task]>) in
            
            let statusCode = ((response.response?.statusCode))!
            
            if statusCode == 200 {
                
                self.tasks.append(contentsOf: response.result.value!)
                var previewSection:String?
                
                for task in self.tasks {
                    
                    if task.status != "finalizado" && (previewSection != task.dateRealization || previewSection == nil){
                        previewSection = task.dateRealization
                        var tsksOfSection = [Task]()
                        self.sections.append(task.dateRealization)
                        
                        for task in self.tasks {
                            if task.dateRealization == previewSection && task.status != "finalizado" {
                            tsksOfSection.append(task)
                            }
                        }
                        self.items.append(tsksOfSection)
                        
                    }
                }
                self.actionPlanTaskTableView.reloadData()
            }else
            {
                AlertUtil.showAlert(title: "Falha",message: "Nao foi possivel carregar as tarefas", context: self)
            }
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "actionPlanCell", for: indexPath) as! ActionPlanTaskTableViewCell
        cell.taskNameLabel.text = self.items[indexPath.section][indexPath.row].descricao
        let checkbox = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0))
        checkbox.boxType = M13Checkbox.BoxType.square
        checkbox.secondaryTintColor = UIColor.darkGray
        cell.finalizationTaskView.addSubview(checkbox)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < sections.count {
            return sections[section]
        }
        
        return nil
    }
    
}
